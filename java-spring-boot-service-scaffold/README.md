# Java Spring Boot Scaffold

This module aims to serve as the starting point for Java Services.

## Getting Started

To create a new Java Spring Boot Project:

1. Clone repository and create your service folder:
    ```shell
    git clone git@gitlab.com/dctx/devcon-contact-tracer.git
    cp java-spring-boot-service-scaffold my-service
    cd my-service
    ```

1. Edit `pom.xml` with your service `artifactId` + other details

    ```$xslt
    <groupId>org.dctx.devconcontacttracer</groupId>
    <artifactId>my-service</artifactId>
    <version>1.0.0</version>
    <name>Your Service Name</name>
    <description>Your Service Description</description>
    ```
   
## Running
### Prerequisites
* JVM 11

You should be able to easily run your service using `mvn spring-boot:run`

## Building

Build using standard maven build: `mvn clean install`

## Packaging Docker Image

This scaffold uses Spring Boot 2.3 that adds easy dockerization features. 
To create a docker image run the following:
```shell script
mvn spring-boot:build-image
```

That’s it! Your application has been compiled, packaged and converted to a Docker image. You can test it using:

```shell script
$ docker run -it -p8080:8080 my-service:1.0.0
```

More information found [here](https://spring.io/blog/2020/01/27/creating-docker-images-with-spring-boot-2-3-0-m1).



