package org.dctx.devconcontacttracer.scaffold;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringBootServiceScaffoldApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaSpringBootServiceScaffoldApplication.class, args);
    }

}
