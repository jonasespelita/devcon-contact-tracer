package org.dctx.devconcontacttracer.scaffold;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JavaSpringBootServiceScaffoldApplicationTests {

    @Test
    void contextLoads() {
    }

}
